ARG CI_REGISTRY
FROM ${CI_REGISTRY}/sw4j-net/jdk10/master:latest

ARG VERSION=3.6.0
ARG BASE_URL=https://www-us.apache.org/dist
ARG SHA=fae9c12b570c3ba18116a4e26ea524b29f7279c17cbaadc3326ca72927368924d9131d11b9e851b8dc9162228b6fdea955446be41207a5cfc61283dd8a561d2f

RUN apt-get update && \
    apt-get -y upgrade && \
    apt-get -y install curl && \
    mkdir -p /usr/share/apache-maven && \
    curl -sSL ${BASE_URL}/maven/maven-3/${VERSION}/binaries/apache-maven-${VERSION}-bin.tar.gz -o apache-maven-bin.tar.gz && \
    echo "${SHA}  apache-maven-bin.tar.gz" | sha512sum -c && \
    tar xzCf /usr/share/apache-maven apache-maven-bin.tar.gz --strip-components=1 && \
    ln -s /usr/share/apache-maven/bin/mvn /usr/bin && \
    rm -f apache-maven-bin.tar.gz
